package android.srinivasani.reddit;

import android.net.Uri;

public interface ActivityCallback {
    void onPostSelected(Uri redditPostUri);
}

